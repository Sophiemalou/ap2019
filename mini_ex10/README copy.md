**Individual work**

![screenshot](Untitled Diagram.jpg)

When doing my flowchart, I chose to keep it simple, so that any user will understand how the program works. 
Furthermore, I included a technical mini flowchart, that shows the works and libraries behind the program. 
I found the task fitting with the code, as there is a "goal"; for the user to know about the weather, and therefore it was easy to keep the flowchart simple, yet effective.

The program lies withing the folder in this mini_ex10, and the code + html file is all included. 


**Individual: How is this flow chart different from the one that you had in #3 (in terms of the role of a flow chart)?**

The flowchart I have made is not that different from our groups previous flowchart; as there is an end goal for the users of both programs.
In my flowchart the user is clearly guided with steps, and it has an easy input -> output form. Every time the user enters the name of a city,
weather information will be shown as soon as the submit button is pressed. 
In the group flow chart, the user has the ability to think about their choices, and thereby make more decisions. 

Both programs looks well, and works well in the form of a flowchart, as they guide the users to a possible "end destination" or to the desired goal. 


**Individual: If you have to bring the concept of algorithms from flow charts to a wider cultural context, how would you reflect upon the notion of algorithms?**
When we talked about algorithms in class, i mostly remember a flowchart we were shown; the process of baking a cake. 
It was a very strict and simple "step by step" recipe, and the user didn't have many different choices to make in the process. 
- the only thing they could "decide" was wether the cake was done baking or if it was udnercooked, and had to continue baking. 
I'm unsure as to how algorithms could work in our daily lives, but I'm quite confident, that they could put some perspective on a lot of our daily routines and actions.
As an example, how are we usually going about our grocery shopping, or what do we do when we get home after a long day. 
It would be very interesting to try and turn our seemingly random actions into flowcharts, and see if we could find a pattern? 

As for me, I'm quite sure, that a flowchart overview would show me a lot of patterns that I wasn't aware of. 
- I also think that it would be possible making an algorithm for how I go about cleaning, or even cooking. 

In my oppinion it would be a funny experiment to try and imlement the values of algorithms and flowcharts to our every day lives, as they would bring some simplification and overview. 
- However, I think it would be almost impossible to try and make them "accurate" as we human beings tend to do things that pops into our minds without putting further thoughts into wether or not it's in the right order or at the right time.  


_____________________________________________________________________________



**Group work**


We made 2 ideas in the group, both in the theme of emojis. 

**The first Idea is called; Truemoji**


![screenshot](Truemoji.png)


This program is based on the topic from our second week in AP; "Geometric shapes". 
In this topic we talking about, amongst other things, emojis and their cultural meanings and aspects. 
One of the main talks was about the purpose of emojis and wether or not they should reflect humans in both colour and looks. 
- this is what we have based our first idea upon; what is the signal value of the emojis we send, and how do they affect the way we understand the text messages they accompany. 

In this program the user is presented with a text message, that they can send to any of their friends. For every sentence of the text, the user can choose between a variety of emojis, presented in a dropwodn menu. This gives the user the chance to make the texts more personal, and to chose which emotional context they find fitting for the text. Whenever the user has chosen the emoji they find fitting, they can send the text with a click on the "send" button.
There are 5 categories of feelings tied to each of the sentences we have used in a random order, these can be: happy, sad, sassy, hurt etc. 
The emojis to choose from are the ones currently in the IOS text message app, as they are commonly known. 

We have not yet decided which output or specific outcome we want from this program - as it can move in many directions. 
One of them could be limiting the emojis that can be used in the sentence, so that the emojis will be less misunderstood - and used in less different emotional settings. 
(personal insight, that might not be shared in the group)

Option 1:
The user of the program receives a message: a message back from a clearly hurt recipient


Option 2:
A collage of other users personalized text-messages.
-How did other people perceive the written text?

![screenshot](Truemojiflow.jpg)


**What might be the possible technical challenges for the two ideas and how are you going to solve them?
Technical challenges for the first idea:**



1.  To avoid aesthetic disagreements during the coding process, we will make an aesthetical prototype of our program in Axure (link under the flowchart)

2.  Another predicted technical challenge in this design process will occur if we choose option number 2. If we choose option number 2 the output of the program, will show a collage filled with other peoples messages including your own, to make it possible for the user to compare his/hers message and signal value of the message with others. We would like to be able to capture different users messages, and we actually don’t know how to solve this challenge.

3.  The last technical challenge of the program, will probably be the interactive aspect. For example to connect a button with the different emojis, and let them disappear after the user have chosen a specific emoji.

[Link to a prototype made in Axesure](https://cz17yw.axshare.com/#g=1)



To try and make this program as accurate as possible, we will be doing a field study, consisting of simple interviews with people in our own agegroup ≈ 20-30. All of the emojis used in our program representing the 5 different feelings we have chosen to work with, will be shown to the participants. This way, we can get a better understanding of how people actually use these emojis, and figure out which of them they find most fitting for each of the "feeling-groups"
We expect to reach a conclusion stating, that most people use the emojis differently, and that a variety of emojis from each category has to be available as an option for the user to be able to express themselves accordingly. 

_____________________________________________________________________________

**Our second idea is called; Capmoji**


![screenshot](capmoji.png)


Our second idea is leaning on the first one, presented above. 
It's our solution to how emojis can "reflect" us, or take on a humanlike appearence, and how this can solve common misunderstandings between senders and recipients on online social platforms. 

In our second idea, the user of our program is presented with a textbox, or a message field, in which they are able to compose a message. 
When the user has done so, he or she can press the emoji icon, and a picture of their imediate reaction or face expression is captured. 
This program helps the user get the most fitting emoji to the text that they are writing; their own face expression. When the user has written a message, they can press the emoji button, and a picture of the user is captured, and formated into an original emoji size. This way, there will be less misunderstandings, and the receiver of the text - who probably knows the sender - will not be doubting the true intentions of what's written to them.
When using this type of emoji, the communication between users will be made more "humanlike" as their face will be turned into emojis, communicating their true intentions with the message sent. 



![screenshot](ide2.png)

**What might be the possible technical challenges for the two ideas and how are you going to solve them?**
**Technical challenges for the second idea:**


1.  To shape and make the “emoji” in which your face will show

2.  How to go about the aesthetic limitations of trying to fit a whole face inside the original emoji format.

3.  How to keep the data capture safe, and prevent other users of the app to get hold of your personal smileys. It’s also a matter of copyright? Who has the ownership of the personal emojis? (is it even possible to create this app?)
The last technicality would be how to implement the capmojis in the texts. Our first thougts on how to go about it, is to simply have an emoji button; when pressed, it takes a picture of one’s expression and “emotions” and fits it into the original emoji format. Then we have to get the camera and the message app to work together, and figure out wether to store some of the capmojis as the standards, or wether to always create new ones, deleting the old.

We are going to solve the technical challenges by watching the relevant Shifman-videoes. Furthermore we will be getting help and verbal sparring from our mentors, instructors and classmates.

