![ScreenShot](SS1.png) 
![ScreenShot](SS2.png)

[run me](https://glcdn.githack.com/Sophiemalou/ap2019/raw/master/mini_ex5/empty-example/sketch.js)




When I delivered my previous Mini_ex4, I had some trouble with double video shown, and the fact that the photo capture button didn't work.

I have now changed those factors, so that you can capture pictures, without the coloured background, and get a sense of a raw picture that you can use for various social medias etc.
- the coloured background only serves as a "set the mood" function, in the small "photobooth" you are in.

The syntaxes used for the correction:
I created a second variable for the video, so that I could sepperate the two captures I had. (with the help of Ann karring)

here you can see the syntaxed that I used;
video = createCapture(VIDEO);
video2 = createCapture(VIDEO);
video.size(640,480);
video.hide();
video2.hide();

Furthermore I wanted to be able to capture the picture. I've done this with a syntax that stops and shows a still from the videostream from the webcam

  video2.pause().show();
  
  As you can see on the screenshots, the video can continue and change, and the picture beneath that is captured, stays the same until it's reset. 

  
