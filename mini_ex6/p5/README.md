![ScreenShot](NIF1.png) 
![ScreenShot](NIF2.png)
![ScreenShot](NIF3.png)

[run me](https://gl.githack.com/Sophiemalou/ap2019/raw/master/mini_ex6/p5/example/index.html)

My first thought going into this game creation was to create something classic and iconic, as an ode to the games that have shaped my childhood.

I initially thought that I was going to make a snake game, but after a few takes on it , it became apparent that there weren't many factors, which I could change - and that I didn't have the skill-set to invent a brand new game.

After a lot of going back and forth between games, I decided on a simple - yet fun - game, that had the potential to grow with multiple levels and opponents, making it a bit more challenging for the user.
As of today, the game is a "pick up all the coins to win" game, that is almost impossible to lose. I'm very pleased with this, as I was never focused the competitive sides of games in this task - as I tried to bring a smile on the users face, and enhance the nostalgia and childhood feelings, that I connect to gaming.

My game is based on one of the newest "harry potter" films (well at least the Harry Potter universe). I have taken a Niffler (a little creature with a fondness of gold and shiny things) and given him some abilities.
the niffler as a class can rotate around himself in his chase after the coins, and he is toggled to the x and y position of the mouse.

I found this mini_ex very complex, and was very happy with the result, as it's innocent and fun. I hope the "users" will take a liking to the niffler, and help him on his quest for gold.
If I ever grow into a competent coder, I'll make this game an app with multiple levels and more magic ;-) 
