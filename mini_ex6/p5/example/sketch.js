var coins;
var niffler;
var NifflerImg;
var score = 0; //counter

function preload () {
  NifflerImg = loadImage('thetruecoinstealer.png'); //preloading niffler prior to program bootup
}

function setup() {
  createCanvas(800, 800);
   coins = new Group();
  for (var i = 0; i < 100; i++) { //amount of coins
    var coinsobjects = createSprite(random(100, width-100),random(100, height-100),10, 10);
    coinsobjects.shapeColor = color(255, 255, 0); //RGB
    coins.add(coinsobjects);
  }
  niffler = createSprite(width, height);
  niffler.addImage(NifflerImg);
  drawSprites();
}
function draw() {
  background(50);
  niffler.velocity.x =
    (mouseX-niffler.position.x)*0.1;
  niffler.velocity.y =
    (mouseY-niffler.position.y)*0.1;
  niffler.overlap(coins, getCoin);
  drawSprites();

  fill(255);
  noStroke();
  textSize(72);
  textAlign(CENTER, CENTER);
  if (coins.length > 0) {
    text(score, width/2, height/2);
  }
  else {
    text("you're the thief!", width/2, height/2);
  }
  if (mouseIsPressed) {
    niffler.rotation += 2;
  }
drawSprites();
}

function getCoin(niffler, coin) {
  coin.remove();
  score += 1;
}
