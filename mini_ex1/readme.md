![screenshot](Screen Shot 2019-02-08 at 14.23.07.png)

[link](https://cdn.staticaly.com/gl/Sophiemalou/ap2019/raw/master/mini_ex1/empty-example/index.html)

This project is a reflection of some of the thoughts I left Transmediale with. The last talk I attended was called Creating Commons, and I’ve tried to depict this with small dots, joining together in one bigger one. This is supposed to resemble societies and social circles - creating a sense of belonging and being part of something bigger. 

In this project my main focus was to change some of the references found on P5, and shape and spread the patterns to fit designs I had come up with.   

I started this mini ex, with a reference from P5 called; Vertex, which initially was only 4 small dots in a perfet 2D square. I created, with the help of our instructor Esther, a variable, for looping the dots (using for-loop and variables), and thus scattering them across my window background.  

To begin with I created two variables, starting with the placement of the first 4 dots in the Vertes reference. Var = Variable, for, forloop. These variables looped 10 times as a whole on the Y-axis, and then 20 times per pre-loop on the x-axis. This variable created the gitter effect with the dots as seen on the picture.  

var y=0; 
for( var j=0; j<10; j++){  
y=y+55; 

var x=30; 
  for(var i=0; i<20; i++){ 

    strokeWeight(3); 
  beginShape(POINTS); 
  vertex(x,20+y); 
  vertex(45+x, 20+y); 
  vertex(45+x, 75+y); 
  vertex(x, 75+y); 
  endShape(); 

  x=x+45; 

The strokeweight describes the size of the dots, and the beginShape and endShape defines the reference I used for the computer.  

Right before finishing my picture,  i learned how to better place the objects exactly where i wanted them with the Log console syntax with the Javascrips console seen in line 34. - This helps me to pinpoint the coordinates for the location on the window, where i want to place my figures, with the click of my mouse.  
This helped me find the perfect location for my Circle, so that the smaller dots end behind the bigger circle. 

This process has been very difficult for me, as I’m coding in a mathematical language to create pictures and visual “art”. It’s like mu brain has a hard time linking those two together, but I’m getting there – to the point where I can “see” what I’m writing down in code. 
Coding to me is very different from anything I’ve tried in terms of reading and writing. As the two latter are easier for me to process and make sense of, maybe because they’ve been taught to me from young age. 
