|Describe your program and what you have used and learnt

This is by far the most complicated code I've made so far. This can easily be seen in the way I've handled, or rather mishandled my variables. In the beginning I used a reference from P5.js, which I took apart and tried to understand down to the last bit. This worked very well for me in the beginning, and I got my square faced emojis to move in a very pleasing way.
- Still, at this moment, I didn't have the full control of the movement or the colour graduation...
After puzzling with this for a long time, I got to a place where I was happy with what I had put on my canvas so far, which was a red and a green box (symbolising the round smiley face) and they moved with the mouse on the X-axis.

The hardest part for me through this mini_ex was getting a face onto these squared backgrounds I had intended to use as the faces. I couldn't get my variables to apply to the eyes (small ellipses) or the mouths (vertexes) And this was the cause of a lot of frustration.
- This also led my variables to be a bit random, as I in the end just changed them based on what I saw happening on the canvas, rather than what made sense to have in a variable. I have never been into math, but a fan of the arts, so I guess this will be the way I move forward on my coding journey.

The end result of my smiley faces was almost as I had envisioned it. I have 2 smileys, changing between each other, happy and sad. The only thin that is a bit off, is the movement of the mouth... on both of the emojis actually. This can be interpreted in context of what I focused on in this project, namely emotions.
- they are often fussy, uncontrollable and mixed up -> as is my code.


|What have you learnt from reading the assigned reading? How would you put your emoji into a wider cultural context that concerns identification, identity, race, social, economics, culture, device politics and beyond? (Try to think through the assigned reading and your coding process, and then expand that to your experience and thoughts - this is a difficult task, you may need to spend sometimes in thinking about it)

This project for me was about emotions, and how we use emojies to explain others how we feel through the Internet, or social media. Therefore I decided to go with an emoji that can be happy and sad, small and big - like two emojis who are intertwined, and mixed together.
I think this reflects most of us in this modern era of social media usage - as we often look so happy and polished online, but the truth is that we are far from it. Therefore I've created this "flip" emoji, that can portray the fact that these feelings are not far apart, no online, and certainly not in real life. Everything has a flipside, and there should be room for both happiness and sadness online!

Thinking about what we discussed on class a great deal, namely identifying with emojis, I've chosen to make a red and green square faced emoji. I think that the emotions are the important matter, not so much the skin colour or the shape of the emoji. - My thoughts behind this, was that the emotions are to be communicated, no matter who you are, where you're from or how you look, and in this way we can all relate and avoid a lot of unnecessary trouble.

![ScreenShot](billede1.png)
![ScreenShot](billede2.png)
![ScreenShot](billede3.png)

[Run me](https://gl.githack.com/Sophiemalou/ap2019/raw/master/mini_ex2/p5/example/index.html)