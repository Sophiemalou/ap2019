

function setup() {

  createCanvas(720, 400);
  strokeWeight (3);
  rectMode(CENTER);
}

function draw() {
  background(230);

//Variation r1 = musens x-position ændrer bredde og højde
	// Den værdi jeg vil ændre på har jeg sat til min MouseX, den starter
//ved 0, og slutter når width slutter. På Y-aksen er det tilsvarende.
//Jeg er klar over at variablerne er blevet en del mere komplicerede og random end jeg havde intenderet.

  var r1 = map(mouseX, 0, width, 0, height);
  var r2 = height - r1;

  fill(124,252,0,r1);
  rect(width/2 + r1/2, height/2, r1, r1);

	ellipse(width/2+r1/2-r1/4, height/2-r1/4,r1/10, r1/10); //green
	ellipse(width/2+r1/2+r1/4, height/2-r1/4,r1/10, r1/10);

	push();
	translate(width/2+r1/2-r1/4, height/2+r1/10+75,r1/5,10,r1/4);

	noFill();
beginShape(100); //mouth
	curveVertex(84, 91);
curveVertex(84, -91);
curveVertex(68, -19);
curveVertex(21, -17);
curveVertex(32, -91);
curveVertex(32, 91);

endShape(0);
	pop();

  fill(237,34,93,r2);
  rect(width/2 - r2/2, height/2, r2, r2);

	ellipse(width/2-r2/2-r2/4, height/2-r2/4,r2/10, r2/10); //red
	ellipse(width/2-r2/2+r2/4, height/2-r2/4,r2/10, r2/10);
	push();
	translate(width/2-r2/2-r2/4, height/2-r1/10,r2/5,10,r2/4);

	noFill();
beginShape(100);//mouth
curveVertex(84, 91);
curveVertex(84, 91);
curveVertex(68, 19);
curveVertex(21, 17);
curveVertex(32, 91);
curveVertex(32, 91);
endShape(0);
pop();

}

function mousePressed(){
	console.log(mouseX,mouseY);
}
