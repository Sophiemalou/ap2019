AP2019

![ScreenShot](face.png) 

[run me](https://gl.githack.com/Sophiemalou/ap2019/raw/master/mini_ex4/p5/example/sketch.js)

This assignment was really intriguing for me, as it was the first time I saw how it could meddle with the "real world" and how it could showcase small bits of our life or private time through the webcam, with sound etc.

I have done this with simple syntaxes, first of all by describing my buttons and functions. hereafter by limiting the video seen on the canvas, and creating a background that can be changed with the click on a button. 

I’ve set my mind up to understanding the core concepts of coding, and as I finish some side-projects and get more time on my hands – I plan on returning to these assignments (this one in particular) as I really want to produce a better product than what I have presented these past weeks.

Therefore this code is fairly simple, and not without flaws. My concept was for people to see their faces in a “private” session with the camera, and set a mood with the help of changing background colours. After playing around with this, the user should be able to upload pictures to a various of social medias – not knowing exactly where the picture would end, this being facebook, instagram, snapchat etc. I was intrigued by this private to public moment, as you first have a video séance with yourself, and thereafter immortalise the moment for the world to see- not in control of which online-social-arena receiving your picture.

Another concept of mine was that you would press a button stating that the computer would take a picture after a countdown of 3. But instead of taking the picture after 3 seconds, the computer would wait 6 seconds or more, capturing your face in a “imperfect” state of frustration or wonder – hereafter it would either save the picture to a folder on your computer, named wonders, or post it in a folder on facebook of the same name. I think that it’s important to show another side of ours elves, not only posting pretty staged pictures of ourselves.

I was again limited by my skills, but I hope to be able to make such things in the future – perhaps changing the way we present our selves on social media.
