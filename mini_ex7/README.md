![Screenshot](ant1.png)
![Screenshot](ant2.png)

[run me](https://gl.githack.com/Sophiemalou/ap2019/raw/master/mini_ex7/p5/example/index.html)


I worked with Stine, to make an interpretation on langtons' ants. As the ants react to a basic ruleset  given to them; Turn left or right depending on the background being white or black etc. 


Our program focuses on the fact that we have tried to bend and break the original rule−set of Langton's ants, by changing the visual parameters in the original code, such as colours and sizes. 
We focused on changing and bending the rules instead of trying to make our own. This resulted in a very different visual experience from Langtons' ants. 
What we did was changing size parameters for the grid, and the ants themselves. Furthermore, we tried to enlarge the stroke weight to the extreme, and see what that would look like in our already very bent system. 
The result consists of more random movements than generative, staying matter, as we don't see the patterns made by the ants anymore, we see a cluster of ants,
moving and blurring in motions that draws ones mind towards Perlin noise (but isn't) The cluster of ants seem to create and wipe out each other with an impressive speed. 
an observation; The cluster ant is moving a lot faster after the change of background than the single Langton ant on a white background. 
This might be because the ants don't have the white and black coloured tiles to react too, and therefore defines its state by the random interaction the ants have with each other when their paths cross? 
I couldn't figure out as to why the speed changed, so all of the above is an assumption.  

We enhanced the stroke weight a lot, which resulted in the fact that you couldn't tell it was an ant. This resonated better with generative art and randomnes for me, as well as taking focus away from the original concept; ants. 

The rules served as the artistic and aesthetic part of our project, as they are the ones being bent and broken, 
resulting in randomness and a brand new visual look compared to the original Langton's ants. 

For me, it's definitely more of an art form and a philosophical question, rather than something very stringent.
I think Langtons' ants serves a great example of generative art, as it evolves over time, leaving intricate patterns, formed by a very simple rule−set. 
When I think of automata, generative art and randomness, I think of how simple rule−sets can visually result in something large, complex and ever changing.
This can also be looked at as a social philosophy. The many ants, they create somehting together over time - whereas the singled out ant, with the broken ruleset, isn't fucntioning as well, or leaving any pattern behind.

Personaly I can see this happening in society all around me. Even though it's far fetched to talk about society and social norms through this code, it's something that makes great sense in this modern society. Alone, with no ruleset to guide you, you create and erase yourself over and over and over. 