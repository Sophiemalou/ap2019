AP2019
README 3

https://gl.githack.com/Sophiemalou/ap2019/raw/master/mini_ex3/p5/example/index.html

I keep having the same problem when coding. I want to do too much, and end up with technical difficulties, and a product that wasn’t anywhere near what I wanted.
My visions coming into the creation of a throbber was for it to depict animals, and in this way showcase that time is running out for a lot of endangered species.
- This started out as me envisioning a panda, lying in the centre of a spinning bamboo ring (in 3D). I had envisioned this on top of an imported background, a bamboo forest.

After a lot of trial and error, I found out that the WEBGL file, the 3D bamboo ring and the 2D image couldn’t be shown together in a file, and I started over.
As I had no previous experience with loading images into a code with 3D objects, I continued to try multiple pictures, downloading new libraries and meddled with the code. So to cut this story short, I simply spent all my time trying to make my vision work. I stuck to the frustration, and have learned a thing or two about which objects can be put together.

This leads me to present a very underwhelming throbber, which mainly consists of 3D references from the P5.js library.

In the future I’ll try and make something from scratch, that will work … But for this project, I simply had no more time to spend, and ended up with a product far from what I imagined.
